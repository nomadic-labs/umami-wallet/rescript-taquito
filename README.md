# `Taquito Rescript Bindings`

## Status

⚠️ Work in progress

## Installation

```console
yarn add rescript-taquito
```

`rescript-taquito` should be added to `bs-dependencies` in your
`bsconfig.json`:

```diff
{
  //...
  "bs-dependencies": [
    // ...
+    "rescript-taquito"
  ],
  //...
}
```

## Authors and acknowledgment
Nomadic-Labs (2021-2023)
* @leoparis89     Lev Kovalski   (2023) / Nomadic-Labs
* @sagotch        Julien Sagot   (2023) / Nomadic-Labs
* @comeh          Corentin Méhat (2023) / Nomadic-Labs (CI)

## License
MIT

## Project status
This project is under development for usage in `umami-mobile` and `umami` (desktop) clients.

Standby.
