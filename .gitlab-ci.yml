stages:
 - build
 - test
 - test-coverage
 - sast
 - publish
 - release
 - deploy
 - code-intel

image: node:latest

.extract_values: &extract_values
    # Extract value from `package.json`
    - NPM_PACKAGE_VERSION=$(node -p "require('./package.json').version")
    - NPM_PACKAGE_NAME=$(node -p "require('./package.json').name")
    # Extract value from `bsconfig.json`
    - BS_PACKAGE_VERSION=$(node -p "require('./bsconfig.json').version")
    - BS_PACKAGE_NAME=$(node -p "require('./bsconfig.json').name")
    # - echo "NPM_PACKAGE_VERSION=${NPM_PACKAGE_VERSION}   BS_PACKAGE_VERSION=${BS_PACKAGE_VERSION}"
    # - echo "NPM_PACKAGE_NAME=${NPM_PACKAGE_NAME} BS_PACKAGE_NAME=${BS_PACKAGE_NAME}"

check-consistency:
  stage: .pre
  before_script:
    - *extract_values
  script:
    # echo "Check name and version from bsconfig.json and package.json match"
    - |
      if [[ "${NPM_PACKAGE_VERSION}" != "${BS_PACKAGE_VERSION}" ]]; then 
        echo "ERROR - different package versions in `package.json` and `bsconfig.json`"; 
        exit 1; 
      fi;
    # echo "Check name and version from bsconfig.json and package.json match"
    - |
      if [[ "${NPM_PACKAGE_NAME}" != "${BS_PACKAGE_NAME}" ]]; then 
        echo "ERROR - different package names in `package.json` and `bsconfig.json`"; 
        exit 1; 
      fi;

check-scope-package-names:
  stage: .pre
  before_script:
    - *extract_values
  script:
    # echo "Validate that the package name is properly scoped to the project's root namespace."
    - |
      if [[ ! $NPM_PACKAGE_NAME =~ ^@$CI_PROJECT_ROOT_NAMESPACE/ ]]; then
        echo "Invalid package scope! Packages must be scoped in the root namespace of the project, e.g. \"@${CI_PROJECT_ROOT_NAMESPACE}/${CI_PROJECT_NAME}\""
        echo 'For more information, see https://docs.gitlab.com/ee/user/packages/npm_registry/#package-naming-convention'
        exit 1
      fi

compile:
  stage: build 
  only: 
    - develop 
  before_script:
    - *extract_values
  script: 
    - npm ci # the recommended best practice for CI/CD (as opposed to npm i)
    - npm run build 

# test:
#   stage: test
#   only: 
#     - develop 
#   script:
#     - npm ci # the recommended best practice for CI/CD (as opposed to npm i)
#     - npm run build
#     - npm run test


publish gitlab:
  image: node:15.4
  # Dowgraded from 'latest' for gitlab auth issue : https://forum.gitlab.com/t/npm-publish-fail-on-gitlab-registry-from-gitlab-ci/46901

  stage: publish
  only:
    - main  
  # rules:
    # - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH || $CI_COMMIT_REF_NAME =~ /^v\d+\.\d+\.\d+.*$/
    #   changes:
    #     - package.json
  before_script:
    - *extract_values
  script:
    - echo "--- Publishing to gitlab.com (public) npm registry"

    - npm config set @${CI_PROJECT_ROOT_NAMESPACE}:registry ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/npm/
    - npm config set -- '//gitlab.com/api/v4/projects/${CI_PROJECT_ID}/packages/npm/:_authToken' "${CI_JOB_TOKEN}"
    - npm config list
    # - npm publish
    # # Compare the version in package.json to all published versions (ko with gitlab so far)
    # # If the package.json version has not yet been published, run `npm publish`.
    - |
      if [[ "$(npm view ${NPM_PACKAGE_NAME} versions)" != *"'${NPM_PACKAGE_VERSION}'"* ]]; then
        npm publish
        echo "Successfully published version ${NPM_PACKAGE_VERSION} of ${NPM_PACKAGE_NAME} to GitLab's NPM registry: ${CI_PROJECT_URL}/-/packages"
      else
        echo "Version ${NPM_PACKAGE_VERSION} of ${NPM_PACKAGE_NAME} has already been published, so no new version has been published."
      fi


publish npm:
  # variable NPM_TOKEN must be defined : https://docs.npmjs.com/about-access-tokens
  only:
    - main
  stage: publish
  before_script:
    - *extract_values
  script:
    - echo "--- Publishing to npmjs.org npm registry"
    - npm config set -- '//registry.npmjs.org/:_authToken' "${NPM_TOKEN}"

    - npm config list
    - npm view ${NPM_PACKAGE_NAME} versions

    # Compare the version in package.json to all published versions.
    # If the package.json version has not yet been published, run `npm publish`.
    - |
      if [[ "$(npm view ${NPM_PACKAGE_NAME} versions)" != *"'${NPM_PACKAGE_VERSION}'"* ]]; then
        npm publish --access=public
        echo "Successfully published version ${NPM_PACKAGE_VERSION} of ${NPM_PACKAGE_NAME} to npmjs.org  NPM registry: ${CI_PROJECT_URL}/-/packages"
      else
        echo "Version ${NPM_PACKAGE_VERSION} of ${NPM_PACKAGE_NAME} has already been published, so no new version has been published."
      fi

release_job:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  rules:
    - if: $CI_COMMIT_TAG
      when: never                                  # Do not run this job when a tag is created manually
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH  # Run this job when commits are pushed or merged to the default branch
  dependencies:
    - publish gitlab
    - publish npm
  before_script:
    - apk add --no-cache nodejs npm
    - *extract_values
  script:
    - echo "Running the release job."
    - echo "NPM_PACKAGE_VERSION=${NPM_PACKAGE_VERSION}   BS_PACKAGE_VERSION=${BS_PACKAGE_VERSION}"
    - echo "NPM_PACKAGE_NAME=${NPM_PACKAGE_NAME} BS_PACKAGE_NAME=${BS_PACKAGE_NAME}"
    - echo \
     NPM_PACKAGE_VERSION=${NPM_PACKAGE_VERSION}\
     CI_COMMIT_TAG=$CI_COMMIT_TAG \
     CI_COMMIT_BRANCH=$CI_COMMIT_BRANCH\
     CI_DEFAULT_BRANCH=$CI_DEFAULT_BRANCH\
     CI_PIPELINE_IID=$CI_PIPELINE_IID\
    - release-cli -v
    - release-cli create --description "Release ${NPM_PACKAGE_VERSION} ( pipeline $CI_PIPELINE_IID ) published to gitlab and npm" --tag-name "v${NPM_PACKAGE_VERSION}" --ref "$CI_COMMIT_SHA" --assets-link "{\"url\":\"https://www.npmjs.com/package/@nomadic-labs/rescript-taquito/v/${NPM_PACKAGE_VERSION}\",\"name\":\"npmjs.org registry\",\"link_type\":\"package\"}" --assets-link "{\"url\":\"https://gitlab.com/nomadic-labs/umami-wallet/rescript-taquito/-/packages/\",\"name\":\"gitlab npm registry\",\"link_type\":\"package\"}"
  # release:                                         # See https://docs.gitlab.com/ee/ci/yaml/#release for available properties
  #   tag_name: 'Release v${NPM_PACKAGE_VERSION}'
  #   description: 'Release ${NPM_PACKAGE_VERSION} ( pipeline $CI_PIPELINE_IID ) published to gitlab and npm'
  #   ref: '$CI_COMMIT_SHA'                          # The tag is created from the pipeline SHA.
  #   assets:
  #     links:
  #       - name: "npmjs.org registry"
  #         url: https://www.npmjs.com/package/@nomadic-labs/rescript-taquito/v/${NPM_PACKAGE_VERSION}
  #         link_type: package
  #       - name: "gitlab npm registry"
  #         url: https://gitlab.com/nomadic-labs/umami-wallet/rescript-taquito/-/packages/
  #         link_type: package



#### 

test:
  stage: test
  variables:
    SECURE_LOG_LEVEL: "debug"
  artifacts:
    paths:
    - gl-sast-report.json
    - gl-secret-detection-report.json
    - gl-dependency-scanning-report.json
    # - gl-container-scanning-report.json
    - gl-license-scanning-report.json
    expire_in: 180 days
  script:
    - echo
 
# You can override the included template(s) by including variable overrides
# SAST customization: https://docs.gitlab.com/ee/user/application_security/sast/#customizing-the-sast-settings
# Secret Detection customization: https://docs.gitlab.com/ee/user/application_security/secret_detection/#customizing-settings
# Dependency Scanning customization: https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#customizing-the-dependency-scanning-settings
# Note that environment variables can be set in several places
# See https://docs.gitlab.com/ee/ci/variables/#cicd-variable-precedence

include:
  - template: Jobs/Dependency-Scanning.gitlab-ci.yml
  - template: Jobs/License-Scanning.gitlab-ci.yml
  - template: Security/Secret-Detection.gitlab-ci.yml
  - template: Security/SAST.gitlab-ci.yml
  - template: Security/License-Scanning.gitlab-ci.yml
  - template: Security/Dependency-Scanning.gitlab-ci.yml
  - template: Security/SAST-IaC.gitlab-ci.yml
  # - template: Security/Container-Scanning.gitlab-ci.yml

# container_scanning:
#   variables:
#     GIT_STRATEGY: fetch
#     DOCKERFILE_PATH: "Dockerfile"
#     # DOCKER_IMAGE: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA
#     DOCKER_IMAGE: ${CONTAINER_NAME}
#     SECURE_LOG_LEVEL: "debug"
#   # before_script:
#   #   - export DOCKER_IMAGE="$CI_REGISTRY_IMAGE/$CI_COMMIT_BRANCH:$CI_COMMIT_SHA"
#     # - |
#     #   if [ "$CI_COMMIT_BRANCH" == "$CI_DEFAULT_BRANCH" ]; then
#     #     export DOCKER_IMAGE="$CI_REGISTRY_IMAGE:$CI_COMMIT_SHA"
#     #   fi
#   artifacts:
#     paths:
#     - gl-container-scanning-report.json
#     expire_in: 1 year

secret_detection:
  stage: test

sast:
  stage: test
  artifacts:
    paths:
    - gl-sast-report.json
    - gl-secret-detection-report.json
    - gl-dependency-scanning-report.json
    - gl-container-scanning-report.json
    - gl-license-scanning-report.json
    expire_in: 1 year

license_scanning:
  artifacts:
    reports:
      license_scanning: gl-license-scanning-report.json

publish npm_initial:
  # variable NPM_TOKEN must be defined : https://docs.npmjs.com/about-access-tokens
  only:
    - npm-initial-publish
  stage: publish
  before_script:
    - *extract_values
  script:
    - echo "--- Publishing to npmjs.org npm registry"
    - npm config set -- '//registry.npmjs.org/:_authToken' "${NPM_TOKEN}"
    - npm config list
    - npm publish --access=public
    - echo "Successfully published version ${NPM_PACKAGE_VERSION} of ${NPM_PACKAGE_NAME} to npmjs.org  NPM registry"

